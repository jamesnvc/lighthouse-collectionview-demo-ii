//
//  ViewController.m
//  collectingstuff
//
//  Created by James Cash on 14-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "SpiralLayout.h"

@interface ViewController () {
    UICollectionViewFlowLayout *bigLayout;
    UICollectionViewFlowLayout *smallLayout;
    SpiralLayout *spiralLayout;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    bigLayout = [[UICollectionViewFlowLayout alloc] init];
//    bigLayout.itemSize = CGSizeMake(150, 75);
    bigLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 100);
    bigLayout.footerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 50);
    bigLayout.minimumInteritemSpacing = 150;


    smallLayout = [[UICollectionViewFlowLayout alloc] init];
//    smallLayout.itemSize = CGSizeMake(100, 50);
    smallLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 50);
    smallLayout.footerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 25);
    smallLayout.minimumInteritemSpacing = 25;

    spiralLayout = [[SpiralLayout alloc] init];

    self.collectionView.collectionViewLayout = smallLayout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *nextLayout;
    if (self.collectionView.collectionViewLayout == bigLayout) {
        nextLayout = smallLayout;
    } else if (self.collectionView.collectionViewLayout == smallLayout) {
        nextLayout = spiralLayout;
    } else {
        nextLayout = bigLayout;
    }
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self.collectionView setCollectionViewLayout:nextLayout animated:YES];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    UILabel *cellTitle = (UILabel*)[cell viewWithTag:42];
    cellTitle.text = [NSString stringWithFormat:@"Item: %ld", (long)indexPath.item];
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        return header;
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footerView" forIndexPath:indexPath];
        return footer;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat multipler;
    if (collectionViewLayout == bigLayout) {
        multipler = 20;
    } else {
        multipler = 10;
    }
    return CGSizeMake(50 + multipler * indexPath.item, 50 + multipler * indexPath.item);
}

@end
