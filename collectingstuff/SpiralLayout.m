//
//  SpiralLayout.m
//  collectingstuff
//
//  Created by James Cash on 14-10-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "SpiralLayout.h"

@interface SpiralLayout () {
    NSMutableArray *points;
}

@end

@implementation SpiralLayout

- (void)prepareLayout
{
    NSMutableArray *tmpPoints = [[NSMutableArray alloc] init];
    NSInteger nItems = 0;
    for (int i = 0; i < [self.collectionView numberOfSections]; i++) {
        nItems += [self.collectionView numberOfItemsInSection:i];
    }
    CGPoint center = self.collectionView.center;
    CGFloat Δr = 25;
    CGFloat θ = M_PI / 4;
    for (int i = 0; i < nItems; i++) {
        CGFloat x = center.x + Δr * i * sin(θ * i);
        CGFloat y = center.y + Δr * i * cos(θ * i);
        CGPoint point = CGPointMake(x, y);
        [tmpPoints addObject:[NSValue valueWithCGPoint:point]];
    }
    points = [[NSMutableArray alloc] init];
    for (int secIdx = 0; secIdx < [self.collectionView numberOfSections]; secIdx++) {
        NSMutableArray *sectionPoints = [[NSMutableArray alloc] init];
        for (int itemIdx = 0; itemIdx < [self.collectionView numberOfItemsInSection:secIdx]; itemIdx++) {
            NSValue *point = [tmpPoints objectAtIndex:0];
            [tmpPoints removeObjectAtIndex:0];
            [sectionPoints addObject:point];
        }
        [points addObject:sectionPoints];
    }
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray * attrs = [[NSMutableArray alloc] init];
    for (int secIdx = 0; secIdx < self.collectionView.numberOfSections; secIdx++) {
        for (int itemIdx = 0; itemIdx < [self.collectionView numberOfItemsInSection:secIdx]; itemIdx++) {
            NSIndexPath *idxPath = [NSIndexPath indexPathForItem:itemIdx inSection:secIdx];
            [attrs addObject:[self layoutAttributesForItemAtIndexPath:idxPath]];
        }
    }
    return attrs;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    NSArray *sectionPoints = [points objectAtIndex:indexPath.section];
    NSValue *pointValue = [sectionPoints objectAtIndex:indexPath.item];
    CGPoint point = [pointValue CGPointValue];
    attrs.center = point;
    attrs.size = CGSizeMake(50, 50);
    return attrs;
}

@end
